class BaseClass {
  constructor() {
    this.name = "Thierry";
  }

  name() {
    return this.name;
  }
}

module.exports = BaseClass;
